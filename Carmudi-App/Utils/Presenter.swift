import Foundation

protocol Presenter{
    
    func detachView()
}
