import Foundation

protocol View {
    func showError(message : String)
    func showProgress()
    func hideProgress()
}
