

import Foundation

protocol DashboardView : View{
    
}

class DashboardPresenter : Presenter{
    
    
    fileprivate var service : dashboardService
    fileprivate var view : DashboardView?
    
    
    init(service:dashboardService){
        self.service = service
    }
    
    func attachView(view: DashboardView){
        self.view = view
    }
    func getData(){
        service.getDataRequest(page: 2, maxItem: 2) {  (json, error) in
            if error == nil{
                
            }
        }
   
    }
    func detachView() {
        view = nil
    }
    
}
